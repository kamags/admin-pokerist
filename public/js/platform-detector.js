window.system = new (function PlatformDetector() {
	var dpr = window.devicePixelRatio || 1,
		width = Math.min(window.screen.height, window.screen.width);

	this.FF_TABLET = 'tablet';
	this.FF_DESKTOP = 'desktop';
	this.FF_MOBILE = 'mobile';

	this.PLATFORM_IOS = 'ios';
	this.PLATFORM_ANDROID = 'android';
	this.PLATFORM_WIN = 'win';
	this.PLATFORM_MAC = 'mac';
	this.PLATFORM_LIN = 'lin';
	this.PLATFORM_UNKNOWN = 'unknown';

	// widths for devices
	this.MOBILE_WIDTH = 500;
	this.TABLET_WIDTH = 800;

	this.platform = null;
	this.formFactor = null;

	if (navigator.userAgent.match(/iPhone|iPod/i)) {
		this.formFactor = this.FF_MOBILE;
		this.platform = this.PLATFORM_IOS;
	} else if (navigator.userAgent.match(/iPad/i)) {
		this.formFactor = this.FF_TABLET;
		this.platform = this.PLATFORM_IOS;
	} else if (navigator.userAgent.match(/Android/i)) {
		this.formFactor = this.FF_MOBILE;
		this.platform = this.PLATFORM_ANDROID;

		if (navigator.userAgent.match(/Android 2\./i) && dpr > 1) {
			width = width * dpr;
		} else if (navigator.userAgent.match(/Android 4\./i) && dpr > 1) {
			width = width / dpr;
		}

		if (width >= this.MOBILE_WIDTH) {
			this.formFactor = this.FF_TABLET;
		}
	} else if (navigator.userAgent.match(/Macintosh|Mac OS X/i)) {
		this.formFactor = this.FF_DESKTOP;
		this.platform = this.PLATFORM_MAC;
	} else if (navigator.userAgent.match(/Linux/i)) {
		this.platform = this.PLATFORM_LIN;

		if (width < this.MOBILE_WIDTH) {
			this.formFactor = this.FF_MOBILE;
		} else if (width >= this.MOBILE_WIDTH && width <= this.TABLET_WIDTH) {
			this.formFactor = this.FF_TABLET;
		} else {
			this.formFactor = this.FF_DESKTOP;
		}
	} else if (navigator.userAgent.match(/Windows/i)) {
		this.formFactor = this.FF_DESKTOP;
		this.platform = this.PLATFORM_WIN;
	} else {
		this.formFactor = this.FF_DESKTOP;
		this.platform = this.PLATFORM_UNKNOWN;
	}

	var self = this;
	this.isMobile = function () {
		return self.formFactor == self.FF_MOBILE;
	}
})();
