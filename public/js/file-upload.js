

$(function () {
    'use strict';
   
    var image_model = $('#imageupload #model-name').val();
    var image_id = $('#imageupload #model-id').val();
    
    if (image_id != null) {
	var imageupload = $('#imageupload');


	imageupload.fileupload({
	    autoUpload: true,
	    maxNumberOfFiles: 1,
	    acceptFileTypes:  /(png)|(jpg)|(jpeg)|(bmp)|(gif)$/i,
	    url: '/fileupload/'+ image_model + '/' + image_id
	});

	imageupload.fileupload(
	    'option',
	    'redirect',
	    window.location.href.replace(
		/\/[^\/]*$/,
		'/cors/result.html?%s'
	    )
	);

	imageupload.addClass('fileupload-processing');
	$.ajax({
	    url: imageupload.fileupload('option', 'url'),
	    dataType: 'json',
	    context: imageupload[0]
	}).always(function () {
	    $(this).removeClass('fileupload-processing');
	}).done(function (result) {
	    $(this).fileupload('option', 'done')
		.call(this, $.Event('done'), {result: result});
	});
	
    }
    
    
    var icon_model = $('#iconupload #model-name').val();
    var icon_id = $('#iconupload #model-id').val();
    
    if (icon_id != null) {
	var iconupload = $('#iconupload');
	

	iconupload.fileupload({
	    autoUpload: true,
	    maxNumberOfFiles: 1,
	    acceptFileTypes:  /(png)|(jpg)|(jpeg)|(bmp)|(gif)|(ico)$/i,
	    url: '/fileupload/'+ icon_model + '/' + icon_id
	});

	iconupload.fileupload(
	    'option',
	    'redirect',
	    window.location.href.replace(
		/\/[^\/]*$/,
		'/cors/result.html?%s'
	    )
	);

	iconupload.addClass('fileupload-processing');
	$.ajax({
	    url: iconupload.fileupload('option', 'url'),
	    dataType: 'json',
	    context: iconupload[0]
	}).always(function () {
	    $(this).removeClass('fileupload-processing');
	}).done(function (result) {
	    $(this).fileupload('option', 'done')
		.call(this, $.Event('done'), {result: result});
	});
	
    }
    

});
