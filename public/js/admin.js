
    
$(function () {
    
	

    
	var stopPageReload = function (event, message) {
		event.preventDefault();
		alert(message);
		exit();    
	}
	
	// callback after deleting a quest answer
	var cleanI18nAnswers = function () {
                var $this = $(this),
			index = $this.closest('.js-active-answer').index('.js-active-answer'),
			$parent = $this.closest('tr').length? $this.closest('tr'): $this.closest('.js-active-answer');

		$parent.fadeOut(function () {
			$(this).remove();
		});
		$('.tab-pane:not(.active)').each(function() {
			$(this).find('.js-active-answer').eq(index).remove();
		});
	};

	// callback after deleting i18n image in post
	var deleteThisImage = function () {
		var $this = $(this),
			index = $this.closest('.js-active-answer').index(),
			defaultLanguageImageSource = $('#en').find('.js-active-answer').eq(index).find('img').attr('src');

		$this.closest('.js-active-answer').find('img').attr('src', defaultLanguageImageSource);
	};

	// callback after deleting a line in a record list
	var deleteCurrentLine = function () {
		$(this).closest('tr').fadeOut();
	};
	
	var hash = window.location.hash ;
	
	if (hash != '') {
	    $('.tab-pane').removeClass('active') ;
	    $(hash).addClass('active');

	    $('.nav li').removeClass('active') ;
	    $(hash + '_tab').addClass('active');
	}
	
	$('body')
	

		    .on('change', '#ScenarioPoints_scenario_step_id', function (e) {
			var id = $(this).val();
			var url = '/scenario-steps/getImageURL/'+id;
		
			$.ajax(url, {
			   type: 'POST',
			   async: true,
			   success: function (data) {
			      
				       $('.points-image').attr('src', data);
			       
				   }
			       });
		
		
		    })
	
	
	
		    .on('click', '.points-image', function (e) {
			
			    var screenImage = $(this);

			    var tmpImage = new Image();
			    tmpImage.src = screenImage.attr("src");

			    var realWidth = tmpImage.width;
			    var realHeight = tmpImage.height;

			    var width = screenImage.width();
			    var height = screenImage.height();

			    var x = e.pageX - this.offsetLeft;
			    var y = e.pageY - this.offsetTop;

			    var realX = (x * realWidth) / width;
			    var realY = (y * realHeight) / height;

			    realX = realX.toFixed(0);
			    realY = realY.toFixed(0);

			    $('#ScenarioPoints_y').val(realY);
			    $('#ScenarioPoints_x').val(realX);
			
		     })
		     
		    .on('click', '#scenario-step-tab a', function (e) {
			e.preventDefault();
			$(this).tab('show');
		     })


		// add infinite inputs or images in forms
		.on('click', '.js-add-element', function () {
			var $clonedElem = $(this).closest('.js-new-element').last();

			$clonedElem.clone().insertAfter($clonedElem).find(':text').val('');
			return false;
		})
		
		.on('change', '.link-scenarios', function () {
		   var val = $('.link-scenarios').val();
		   if (val == '0') val = '';
		   document.location.href = "/scenario-links/" + val ;
		})
		
		.on('change', '.point-scenarios', function () {
		   var val = $('.point-scenarios').val();
		   if (val == '0') val = '';
		   document.location.href = "/scenario-points/" + val ;
		})
		
		.on('change', '.step-scenarios', function () {
		   var val = $('.step-scenarios').val();
		   if (val == '0') val = '';
		   document.location.href = "/scenario-steps/" + val ;
		})
		
		.on('click', '.step-move', function () {
			var id = $(this).attr('data-id');
		
			var direction = 'up';
			
			if ($(this).hasClass('step-move-down')) {
			    direction = 'down';   
			}
			var url = '/scenario-steps/move/' + id + '/' + direction;
		
			$.ajax(url, {
			   type: 'POST',
			   async: true,
			   success: function (data) {
				       window.location.reload();
				   }
			       });
			    return false;
			       

		})


		// send links via ajax without params
		.on('click', '.js-simple-send', function () {
			var that = this;
			var $this = $(this);
			var url = $this.attr('href');

			if (confirm('Удалить запись?')) {
				$.get(url, function (data) {
					if (parseInt(data) === 1) {

						// evaluate a callback
						var callback = $this.data('callback');
						if (callback && eval('(typeof ' + callback + '=="function");')) {
							eval(callback + '.apply(that)');
						}
					} else {
						alert('Ошибка!');
					}
				});
			}
			return false;
		});

//	// tiny mce editor
//	tinymce.PluginManager.add('ranks', function (editor, url) {
//		// Add a button that opens a window
//		editor.addButton('hearts', {
//			text: '♥',
//			icon: false,
//			onclick: function () {
//				editor.selection.setContent('♥');
//			}
//		});
//		editor.addButton('clubs', {
//			text: '♣',
//			icon: false,
//			onclick: function () {
//				editor.selection.setContent('♣');
//			}
//		});
//		editor.addButton('spades', {
//			text: '♠',
//			icon: false,
//			onclick: function () {
//				editor.selection.setContent('♠');
//			}
//		});
//		editor.addButton('diamonds', {
//			text: '♦',
//			icon: false,
//			onclick: function () {
//				editor.selection.setContent('♦');
//			}
//		});
//	});

//	tinymce.init({
//		selector: 'textarea:not(.not-wysiwyg)',
//		toolbar1: 'newdocument | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code link image | cut copy paste',
//		toolbar2: 'undo redo | bold italic underline blockquote subscript superscript | formatselect removeformat | hearts clubs spades diamonds',
//		plugins: 'link image lists table ranks code'
//	});
//
//	// datepicker
//	$('.js-datepicker').datepicker({
//		dateFormat: 'yy-mm-dd 00:00:00'
//	});
});