# config valid only for Capistrano 3.2.1
# lock '3.2.1'

# Stuff from .capistrano/tasks/submodule_strategy.rb
set :git_strategy, SubmoduleStrategy

# :application is required for some capistrano stuff.
# git-ssh.sh is uploaded to /tmp/#{application}, for example.
set :application, 'new.admin.pokerist.com'
set :repo_url, 'git@gitlab.kama.gs:pokerist/new-admin-pokerist-com.git'

set :branch, ENV["BRANCH"] || "master"

set :scm, :git
set :format, :pretty
set :log_level, :debug
set :pty, true
set :keep_releases, 5

# set :linked_files, %w{protected/config/database.php}
# set :linked_files, %w{protected/config/production.php}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system protected/runtime}}
set :linked_dirs, %w{protected/runtime public/scenarios}

# in 3.1 custom namespaces should come before the :deploy
namespace :yii do
	desc 'Yii db migrations.'
	task :migrations do
		on roles(:app) do
				execute "APPLICATION_MODE=#{fetch(:stage)} #{release_path}/protected/yiic migrate --interactive=0"
		end
	end
end

namespace :deploy do
	desc 'Restart php-fpm.'
	task :restart do
		on roles(:app), in: :sequence, wait: 5 do
			# execute :killall, "-vu #{host.user} -SIGHUP php-fpm"
		end
	end
	# after :publishing, 'yii:migrations'
	# after 'yii:migrations', :restart
end
