<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
		'components' => [
			'db'          => [
				'connectionString' => 'mysql:host=127.0.0.1;dbname=pokerist_db',
				'emulatePrepare'   => true,
				'username'         => 'minin',
				'password'         => 'd57ghfghfgh9U',
				'charset'          => 'utf8',
			],
			'log'         => [
				'class'  => 'CLogRouter',
				'routes' => [
					[
						'class'  => 'CFileLogRoute',
						'levels' => 'error, warning, info, trace',
					],
				[
					'class' => 'CWebLogRoute',
					'levels' => 'error, warning, info, trace',
				],
				],
			],
			'pokeristApi' => [
				'class'           => 'application.components.PokeristApiComponent',
				'playerApiConfig' => [
					'server'   => 'http://localhost:8080/',
					'username' => 'larry',
					'password' => 'acrobat',
				],
				'adminApiConfig'  => [
					'server'   => 'http://localhost:8080/',
					'username' => 'larry',
					'password' => 'acrobat',
				],
			],
		],
		'params'     => [
			'feedbackNotify' => ['sergey@larionov.biz' => 'Sergey Larionov'],
			'staticHost'     => '//static.pokerist.dev',
		],
	]
);

return $config;
