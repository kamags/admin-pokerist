<?php

Yii::setPathOfAlias('root', realpath(__DIR__ . '/../..'));
Yii::setPathOfAlias('publicPath', realpath(__DIR__ . '/../../public'));

return [
	'basePath'       => __DIR__ . DIRECTORY_SEPARATOR . '..',
	'name'           => 'Pokerist',

	// preloading 'log' component
	'preload'        => ['log'],

	// autoloading model and component classes
	'import'         => [
		'application.controllers.BaseController',
		'application.controllers.BaseAdminController',
		'application.controllers.AdminPostController',
		'application.models.shared.*',
		'application.models.*',
		'application.components.*',
		'ext.fs-tools.*',
		'ext.file-upload.*',
	    	'ext.navigation.*', 	
	],

	'language'       => 'en',
	'sourceLanguage' => 'en',

	// application components
	'components'     => [
		'urlManager'    => [
			'urlFormat'        => 'path',
			'caseSensitive'    => true,
			'matchValue'       => true,
			'showScriptName'   => false,
			'urlSuffix'        => '',
			'useStrictParsing' => true,
			'rules'            => require('_routes.php'),
		],
		'db'            => [
			'connectionString' => 'mysql:host=localhost;dbname=db',
			'emulatePrepare'   => true,
			'username'         => 'root',
			'password'         => '',
			'charset'          => 'utf8',
		],
		'errorHandler'  => [
			'errorAction' => 'main/error',
		],
		'log'           => [
			'class'  => 'CLogRouter',
			'routes' => [
				[
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				],
			],
		],
		'viewRenderer'  => [
			'class'         => 'ext.ETwigViewRenderer',
			'twigPathAlias' => 'ext.twig-renderer.lib.Twig',
			'fileExtension' => '.twig',
			'paths'         => [
				'__main__' => 'application.views',
			],
			'functions'     => [
				'dump' => 'var_dump',
			],
			'globals'       => [
				'Yii' => 'Yii',
				'Nav' => 'Navigation'
			],
		],
		'storageSrc'    => [
			'class' => 'application.components.FileStorageComponent',
			'path'  => 'root.storage',
		],
		'storageCached' => [
			'class' => 'application.components.FileStorageComponent',
			'path'  => 'root.public.images.s',
		],
		'imageRenderer' => [
			'class'         => 'application.components.ImageRendererComponent',
			'config'        => [
				'small'             => ['width' => 90, 'height' => 60, 'method' => 'FSImage::RESIZE_METHOD_FILL'],
				'large'             => ['width' => 1280, 'height' => 1024],
				'quest'             => ['width' => 500, 'height' => 330],
				'avatar'            => ['width' => 120, 'height' => 120],
				'icon'              => ['width' => 180, 'height' => 120, 'method' => 'FSImage::RESIZE_METHOD_FIT_HEIGHT'],
				'promo'             => ['width' => 920, 'height' => 240, 'method' => 'FSImage::RESIZE_METHOD_FILL'],
				'postDetailSmall'   => ['width' => 150, 'height' => 120, 'method' => 'FSImage::RESIZE_METHOD_FILL'],

				'screenshot-iphone' => ['width' => 369, 'height' => 246, 'method' => 'FSImage::RESIZE_METHOD_FILL'],
//				'screenshot-iphone' => ['width' => 480, 'height' => 320, 'method' => 'FSImage::RESIZE_METHOD_FILL'],
//				'screenshot-iphone' => ['width' => 409, 'height' => 273, 'method' => 'FSImage::RESIZE_METHOD_FILL'],
			],
			'storageSource' => 'storageSrc',
			'storageCached' => 'storageCached',
		],
		'user' => [
			'class'          => 'application.components.WebUser',
			'loginUrl'       => ['adminUsers/signIn'],
			'allowAutoLogin' => true,
			'identityCookie' => [
				'path'   => '/',
				'domain' => '.' . (array_key_exists('HTTP_HOST', $_SERVER)? $_SERVER['HTTP_HOST']: null),
			],
			'authTimeout'    => 7776000, // 3 month in seconds
		],
		'authManager' => [
			'class'        => 'application.components.AuthManager',
		],
		'cache' => [
			'class' => 'CFileCache',
		],
	],

	'params'         => [
		'adminEmail'            => 'a.lukin@zmeke.com',
		'supportEmail'          => 'support@pokerist.com',
		'languages'             => require('_languages.php'),
		'adminBlogPostsPerPage' => 50,
		'adminQuestionsPerPage' => 50,
		'adminPromoPerPage'     => 50,
	],
];
