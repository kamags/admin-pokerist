<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
	'components' => [
		'db' => [
			'connectionString' => 'mysql:host=127.0.0.1;dbname=pokerist_new',
			'emulatePrepare' => true,
			'username' => 'pokerist',
			'password' => 'H3UcTXGJZzQ9NMy9',
			'charset' => 'utf8',
		],
		'log' => [
			'class' => 'CLogRouter',
			'routes' => [
				[
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning, info, trace',
				],
//				[
//					'class' => 'CWebLogRoute',
//					'levels' => 'error, warning, info, trace',
//				],
			],
		],
	],
	'params' => [
		'feedbackNotify' => ['sergey@larionov.biz' => 'Sergey Larionov'],
	],
]
);

return $config;
