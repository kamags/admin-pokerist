<?php

return array(
	'/'													=> 'main/index',

	// admin
	'/sign-in'												=> 'adminUsers/signIn',
	'/sign-out'												=> 'adminUsers/signOut',
	
	'/fileupload/<model:[\w.-]+>/'										=> 'main/fileupload',
	'/fileupload/<model:[\w.-]+>/<id:\d+>'									=> 'main/fileupload',

    	// admin scenarios
	'/scenarios'												=> 'adminScenarios/index',
	'/scenarios/edit/<id:\d+>'										=> 'adminScenarios/edit',
	'/scenarios/add'											=> 'adminScenarios/add',
	'/scenarios/delete/<id:\d+>'										=> 'adminScenarios/delete',
    
        // admin scenario-steps
	'/scenario-steps'											=> 'adminScenarioSteps/index',
	'/scenario-steps/<scenario_id:\d+>'									=> 'adminScenarioSteps/index',
	'/scenario-steps/edit/<id:\d+>'										=> 'adminScenarioSteps/edit',
    	'/scenario-steps/move/<id:\d+>/<direction:down|up>'							=> 'adminScenarioSteps/move',
	'/scenario-steps/add'											=> 'adminScenarioSteps/add',
	'/scenario-steps/delete/<id:\d+>'									=> 'adminScenarioSteps/delete',
	'/scenario-steps/getImageURL/<id:\d+>'									=> 'adminScenarioSteps/getImageURL',
    
        // admin scenario-links
	'/scenario-links'											=> 'adminScenarioLinks/index',
	'/scenario-links/<scenario_id:\d+>'									=> 'adminScenarioLinks/index',	
	'/scenario-links/edit/<id:\d+>'										=> 'adminScenarioLinks/edit',
	'/scenario-links/add/<scenario_step_id:\d+>'								=> 'adminScenarioLinks/add',
	'/scenario-links/delete/<id:\d+>'									=> 'adminScenarioLinks/delete',
    
        // admin scenario-images
	'/scenario-images'											=> 'adminScenarioImages/index',
	'/scenario-images/edit/<id:\d+>'									=> 'adminScenarioImages/edit',
	'/scenario-images/add'											=> 'adminScenarioImages/add',
	'/scenario-images/delete/<id:\d+>'									=> 'adminScenarioImages/delete',
    
        // admin scenario-points
	'/scenario-points'											=> 'adminScenarioPoints/index',
    	'/scenario-points/<scenario_id:\d+>'									=> 'adminScenarioPoints/index',	
	'/scenario-points/edit/<id:\d+>'									=> 'adminScenarioPoints/edit',
	'/scenario-points/add/<scenario_step_id:\d+>'   							=> 'adminScenarioPoints/add',
	'/scenario-points/delete/<id:\d+>'									=> 'adminScenarioPoints/delete',

	// admin users
	'/users'												=> 'adminUsers/index',
	'/users/edit/<id:\d+>'											=> 'adminUsers/edit',
	'/users/add'												=> 'adminUsers/add',
	'/users/delete/<id:\d+>'										=> 'adminUsers/delete',


);
