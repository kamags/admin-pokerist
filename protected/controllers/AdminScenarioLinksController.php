<?php

class AdminScenarioLinksController extends BaseAdminController {

	public function accessRules() {
		return [
			['allow', 'roles' => ['admin', 'moderator']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex($scenario_id = null) {

		if ($scenario_id !== null) {
	    
			$scenario_links = ScenarioLinks::model()
				->with('scenario_steps')
				->findAll([
				    'condition'=>'scenario_steps.scenario_id=:scenario_id', 
				    'params'=>[':scenario_id'=>$scenario_id]
				    ]);
			$model = Scenarios::model()->all()->findByPk($scenario_id);
		}
		else {
			$scenario_links = ScenarioLinks::model()->with('scenario_steps')->findAll();
			$model = new Scenarios();
		}
		
		$scenarios = Scenarios::model()->getScenarios();
		$this->render('/scenario-links/index', [
			'model' => $model,
			'scenario_links' => $scenario_links,
			'scenarios' => $scenarios,
		]);
		
		
	}

	public function actionEdit($id) {
	    
		/** @var User $model */
		if (!$model = ScenarioLinks::model()
			->all()
			->findByPk($id)) {
			throw new CHttpException(404, "Scenario not found");
		};
		$scenario_steps    = self::getScenarioSteps();

		$class = get_class($model);
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {

			$model->attributes = $_POST[$class];

			if ($model->save()) {
				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('scenario-steps/edit/' . $model->scenario_step_id . '#links' ));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
		}

		$this->render('/scenario-links/add', [
			'model'     => $model,
			'scenario_steps'     => $scenario_steps,
		]);
	}

	public function actionAdd($scenario_step_id) {
		$model = new ScenarioLinks();
		
		$scenario_steps    = self::getScenarioSteps();

		$class = get_class($model);
		
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {
		    
			$model->attributes = $_POST[$class];
			$model->scenario_step_id = $scenario_step_id;

			if ($model->save()) {

				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('scenario-steps/edit/' . $model->scenario_step_id . '#links'   ));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
			

		}

		$this->render('/scenario-links/add', [
			'model'		    => $model,
			'scenario_steps'     => $scenario_steps,
		]);
	}

	public function actionDelete($id) {
		echo ScenarioLinks::model()
			->deleteByPk($id);
	}
	
	public static function getScenarioSteps() {
		$scenario_steps = [];
		foreach (ScenarioSteps::model()
					 ->all()
					 ->with('scenarios')
					 ->findAll() as $scenario_step) {
			$scenario_steps[$scenario_step->id] = 'Сценарий "' . $scenario_step->scenarios->alias . '" шаг ' . $scenario_step->number ;
		}
		return $scenario_steps;
	}
        
}
