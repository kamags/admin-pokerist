<?php

class MainController extends BaseController {

	public function actionIndex() {
		$this->redirect($this->createUrl('adminScenarios/index'));
	}

	public function actionError() {
		$this->layout = 'error';
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				try {
					$this->render("/error{$error['code']}", ['error' => $error]);
				} catch (CException $e) {
					if ($error['code'] === 403) {
						$this->redirect(Yii::app()->user->loginUrl);
					}
					$this->render("/error", ['error' => $error]);
				}
			}
		}
	}
	
	public function actionFileupload($model, $id=null) {
	    
		$upload_params = array(); 
	    
		$model = strtolower($model);
		
		$path	    = '';
		$delete_url = '';
		$image_id   = '';
		$old_file = null;
		$type = null;
		
		if ($model == 'scenarioimage' || $model == 'scenarioicon') {
			
		    $ScenarioSteps = ScenarioSteps::model()->findByPk($id);
			if (!empty($ScenarioSteps) ) {
				if ($model == 'scenarioimage'){
					$path =  '/scenarios/'  . $ScenarioSteps->scenarios->alias . '/images/';
					$type = '1';
					$delete_url = '/fileupload/scenarioimage/' . $id;
					
				}
			}
			
			$Scenarios = Scenarios::model()->findByPk($id);
			if (!empty($Scenarios) ) {
				if ($model == 'scenarioicon'){
					$path =  '/scenarios/'  . $Scenarios->alias . '/icon/';
					$type = '2';
					$delete_url = '/fileupload/scenarioicon/' . $id;
				}
			}
				
			$ScenarioImages = ScenarioImages::model()->find([
			    'condition'=>'fk_id=:fk_id AND type=:type', 
			    'params'=>[':fk_id'=>$id, ':type'=>$type] 
			]);

			if (!empty($ScenarioImages)) {
				$image_id = $ScenarioImages->id;	
				$old_file = $ScenarioImages->url;
			}
			else {
				$ScenarioImages = new ScenarioImages();
				$ScenarioImages->fk_id = $id;
				$ScenarioImages->type = $type;

				$ScenarioImages->save();
				$image_id = $ScenarioImages->id;

			}
				
			}	
		
		
		
		$upload_params['script_url'] = Yii::app()->getBaseUrl(true) . $path;
		$upload_params['delete_url'] = Yii::app()->getBaseUrl(true) . $delete_url;
		$upload_params['upload_dir'] = Yii::getPathOfAlias('root') . '/public' . $path;
		$upload_params['upload_url'] = Yii::app()->getBaseUrl(true) . $path;
		$upload_params['old_file'] = $old_file;
		$upload_params['new_file_name'] = $image_id;
		   
		$UploadHandler  = new UploadHandler($upload_params); 
		
		
		if (Yii::app()->request->isPostRequest ) {
			$fileparams =  $UploadHandler->options['result'];
			
			if ($model == 'scenarioimage' || $model == 'scenarioicon') {
				$image_path =  $path . $fileparams['files'][0]->name;
				$finalDestination = Yii::getPathOfAlias('root') . '/public' . $image_path;
				if (file_exists($finalDestination)) {
					list($width, $height, $type, $attr) = getimagesize("$finalDestination");
					$ScenarioImages->url = $image_path;
					$ScenarioImages->width = $width;
					$ScenarioImages->height = $height;
					$ScenarioImages->save();

				}
				else {
					$ScenarioImages->delete();   
				}
			}

		}
		
		if (Yii::app()->request->isDeleteRequest ) {
			$ScenarioImages->delete();
		}
		
		
		
	}
	
	

	private static function renderPostImage($blogImageId, $size, $languageId, $postImageClass = 'BlogImage') {
		if (isset(Yii::app()->params['languages'][$languageId])) {
			Yii::app()->language = $languageId;
		}

		$blogImage = $postImageClass::model()
			->findByPk($blogImageId);

		if (!$blogImage) {
			throw new CHttpException(404, "Image not found");
		}

		/** @var $imageRenderer ImageRendererComponent */
		$imageRenderer = Yii::app()->imageRenderer;
		$imageRenderer->render($blogImage->image, $size);
	}

	/**
	 * @param int $blogImageId
	 * @param string $size
	 * @param string $languageId
	 * @throws CHttpException
	 */
	public function actionBlogImage($blogImageId, $size, $languageId) {
		self::renderPostImage($blogImageId, $size, $languageId);
	}

	/**
	 * @param int $blogImageId
	 * @param string $size
	 * @param string $languageId
	 * @throws CHttpException
	 */
	public function actionAcademyImage($blogImageId, $size, $languageId) {
		self::renderPostImage($blogImageId, $size, $languageId, 'AcademyImage');
	}

	/**
	 * @param int $questId
	 * @param string $languageId
	 * @throws CHttpException
	 */
	public function actionQuestImage($questId, $languageId) {
		if (isset(Yii::app()->params['languages'][$languageId])) {
			Yii::app()->language = $languageId;
		}

		/** @var Quest $post */
		$post = Quest::model()
			->includeUnactive()
			->findByPk($questId);

		if (!$post) {
			throw new CHttpException(404, "Question not found");
		}

		$image = $post->image;
		/** @var $imageRenderer ImageRendererComponent */
		$imageRenderer = Yii::app()->imageRenderer;

		try {
			$imageRenderer->render($image, 'quest');
		} catch (FileStorageFileNotFoundException $e) {
			throw new CHttpException(404, $e->getMessage());
		}
	}

	/**
	 * @param int $authorId
	 * @param int $imageId
	 * @throws CHttpException
	 */
	public function actionAvatarImage($authorId, $imageId) {

		/** @var Author $author */
		if (!$author = Author::model()
			->findByPk($authorId)) {
			throw new CHttpException(404, "Author not found");
		};

		$image = $author->image;
		if (!$image || $imageId != $image->id) {
			throw new CHttpException(404, "Avatar not found");
		}


		/** @var $imageRenderer ImageRendererComponent */
		$imageRenderer = Yii::app()->imageRenderer;

		try {
			$imageRenderer->render($image, 'avatar');
		} catch (FileStorageFileNotFoundException $e) {
			throw new CHttpException(404, $e->getMessage());
		}
	}

	/**
	 * @param int $promoBannerId
	 * @param string $languageId
	 * @throws CHttpException
	 */
	public function actionPromoBanner($promoBannerId, $languageId) {
		if (isset(Yii::app()->params['languages'][$languageId])) {
			Yii::app()->language = $languageId;
		}

		/** @var Quest $post */
		$banner = PromoBanner::model()
			->includeUnactive()
			->findByPk($promoBannerId);

		if (!$banner) {
			throw new CHttpException(404, "Banner not found");
		}

		$image = $banner->image;
		/** @var $imageRenderer ImageRendererComponent */
		$imageRenderer = Yii::app()->imageRenderer;

		try {
			$imageRenderer->render($image, 'promo');
		} catch (FileStorageFileNotFoundException $e) {
			throw new CHttpException(404, $e->getMessage());
		}
	}

	public function actionDeviceIcon($categoryAlias, $deviceAlias, $ext) {
		$device = Device::model()->with('icon')->findByAlias($categoryAlias, $deviceAlias);

		if ($device === null) {
			throw new CHttpException(404, "Platform doesn't exist");
		}

		$icon = $device->icon;
		if ($icon === null || $icon->ext != $ext) {
			throw new CHttpException(404, "Unknown icon for platform");
		}

		/** @var $imageRenderer ImageRendererComponent */
		$imageRenderer = Yii::app()->imageRenderer;

		try {
			$imageRenderer->render($icon, 'icon');
		} catch (FileStorageFileNotFoundException $e) {
			throw new CHttpException(404, $e->getMessage());
		}
	}

	public function actionDeviceScreenshot($categoryAlias, $deviceAlias, $screenshotId, $ext) {
		$device = Device::model()->with('screenshots')->findByAlias($categoryAlias, $deviceAlias);

		if ($device === null) {
			throw new CHttpException(404, "Platform doesn't exist");
		}

		if (!isset($device->screenshots[$screenshotId]) || $device->screenshots[$screenshotId]->image->ext != $ext) {
			throw new CHttpException(404, "Screenshot doesn't exist");
		}

		/** @var $imageRenderer ImageRendererComponent */
		$imageRenderer = Yii::app()->imageRenderer;

		try {
			$imageRenderer->render($device->screenshots[$screenshotId]->image, "screenshot-{$deviceAlias}");
		} catch (FileStorageFileNotFoundException $e) {
			throw new CHttpException(404, $e->getMessage());
		}
	}

	// redirect for Facebook
	public function actionTutorialRedirect(){
		$this->redirect('/fb/tutorial.php', 301);
	}
}
