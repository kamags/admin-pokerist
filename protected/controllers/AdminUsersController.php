<?php

class AdminUsersController extends BaseAdminController {

	public function accessRules() {
		return [
			['allow', 'actions' => ['signIn', 'signOut']],
			['allow', 'roles' => ['admin']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex() {
		$this->render('/users/index', [
			'users' => User::model()
						->findAll(),
		]);
	}
	

	public function actionEdit($id) {
		/** @var User $model */
		if (!$model = User::model()
			->findByPk($id)) {
			throw new CHttpException(404, "Пользователь не найден");
		};

		$class = get_class($model);
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {
			// checking the password
			if (isset($_POST[$class]['password']) && $_POST[$class]['password']) {
				$model->password = $_POST[$class]['password'];
			} else {
				unset($_POST[$class]['password']);
			}
			if (isset($_POST[$class]['confirmPassword']) && $_POST[$class]['confirmPassword']) {
				$model->confirmPassword = $_POST[$class]['confirmPassword'];
			} else {
				unset($_POST[$class]['confirmPassword']);
			}

			$model->attributes = $_POST[$class];

			if ($model->save()) {
				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('index'));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
		}

		$this->render('/users/add', [
			'model'     => $model,
		]);
	}

	public function actionAdd() {
		$model = new User();

		$class = get_class($model);
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {
			// checking the password
			if (isset($_POST[$class]['password']) && $_POST[$class]['password']) {
				$model->password = $_POST[$class]['password'];
			}
			if (isset($_POST[$class]['confirmPassword']) && $_POST[$class]['confirmPassword']) {
				$model->confirmPassword = $_POST[$class]['confirmPassword'];
			}

			$model->attributes = $_POST[$class];

			if ($model->save()) {
				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('index'));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
		}

		$this->render('/users/add', [
			'model'     => $model,
		]);
	}

	public function actionDelete($id) {
		echo User::model()
			->deleteByPk($id);
	}
	
	public function actionSignIn() {

		$model = new AdminSignInForm();

		// collect user input data
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('/sign-in', array('model' => $model));
	}

	public function actionSignOut() {
		/** @var $user CWebUser */
		$user = Yii::app()->user;
		$user->logout();
		Yii::app()->request->redirect($this->createUrl('index'));
	}
}
