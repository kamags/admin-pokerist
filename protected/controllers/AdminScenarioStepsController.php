<?php

class AdminScenarioStepsController extends BaseAdminController {

	public function accessRules() {
		return [
			['allow', 'roles' => ['admin', 'moderator']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex($scenario_id = null) {
	    
		if ($scenario_id !== null) {
	    
			$scenario_steps = ScenarioSteps::model()
				//->with('scenarios')
				->findAll([
				    'condition'=>'scenario_id=:scenario_id',
				    'order'=>'number asc',
				    'params'=>[':scenario_id'=>$scenario_id]
				    ]);
			$model = Scenarios::model()->all()->findByPk($scenario_id);
		}
		else {
			$scenario_steps = ScenarioSteps::model()->with('scenarios')->findAll(['order'=>'scenario_id asc, number asc']);
			$model = new Scenarios();
		}
		
		$scenarios = Scenarios::model()->getScenarios();
		$this->render('/scenario-steps/index', [
			'model' => $model,
			'scenario_steps' => $scenario_steps,
			'scenarios' => $scenarios,
		]);
		
		
	}

	public function actionEdit($id) {
	    
		/** @var User $model */
		if (!$model = ScenarioSteps::model()
			->all()
			->with( 'scenario_links')
			->findByPk($id)) {
			throw new CHttpException(404, "Scenario not found");
		};
		
		$scenarios    = self::getScenarios();

		$class = get_class($model);
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {

			$model->attributes = $_POST[$class];

			if ($model->save()) {
				$this->saveScreenshot($model->id);
				
				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('index'));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
		}

		$this->render('/scenario-steps/add', [
			'model'     => $model,
			'scenarios'     => $scenarios,
		]);
	}

	public function actionAdd() {
		$model = new ScenarioSteps();
		
		$scenarios    = self::getScenarios();

		$class = get_class($model);
		
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {
		    
			$model->attributes = $_POST[$class];
			
			$model->number = ScenarioSteps::model()->calcNextNumber($model->scenario_id);

			if ($model->save()) {
			        $this->saveScreenshot($model->id);
			    
				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('index'));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
			

		}

		$this->render('/scenario-steps/add', [
			'model'		=> $model,
			'scenarios'     => $scenarios,
		]);
	}
	
	public function saveScreenshot($scenario_step_id) {
	    
		$model = ScenarioImages::model()->find([
				    'condition'=>'scenario_step_id=:scenario_step_id', 
				    'params'=>[':scenario_step_id'=>$scenario_step_id]
			    ]);
		
		if (count($model) > 0) {
			$finalDestination = Yii::getPathOfAlias('root') . "/public{$model->url}";
			list($width, $height, $type, $attr) = getimagesize("{$finalDestination}");
			$model->width = $width;
			$model->height = $height;
			$model->save();
		}
	}

	public function actionDelete($id) {
	    
		$model = ScenarioSteps::model()
			->all()
			->with('scenario_images')
			->findByPk($id);
		
		
		foreach ($model->scenario_images as $scenario_image) {
			if (!empty($scenario_image->url)) {
				self::deleteImage($scenario_image->url); 
			}
			$scenario_image->delete();
		}

		echo $model->delete();

	}
	
	public function actionMove($id, $direction = 'up') {
		
		$nextNumber = -1;
		
		$currentModel = ScenarioSteps::model()
			->all()
			->findByPk($id);
		
		$currentNumber	    = $currentModel->number;
		$currentScenarioId  = $currentModel->scenario_id;
		
		if ($direction == 'up') {
			$nextNumber = $currentNumber - 1;
		}
		else {
			$nextNumber = $currentNumber + 1;   
		}
		
		$nextModel = ScenarioSteps::model()->find([
				    'condition'=>'scenario_id=:scenario_id AND number=:number', 
				    'params'=>[':scenario_id'=>$currentScenarioId, ':number'=>$nextNumber ]
			    ]);
		
		if (count($nextModel) > 0) {
			$nextModel->number = $currentNumber;
			$currentModel->number = $nextNumber;
			
			$nextModel->save();
			$currentModel->save();
		}
		
		
	    
		//echo $model->number;
	}
	
	public static function getScenarios() {
		$scenarios = [];
		foreach (Scenarios::model()
					 ->all()
					 ->findAll() as $scenario) {
			$scenarios[$scenario['id']] = $scenario['alias'];
		}
		return $scenarios;
	}
	
	public static function actiongetImageURL($id) {
	    
	    $url = '';
	    
	    $ScenarioImage = ScenarioImages::model()->find([
				    'condition'=>'fk_id=:fk_id AND type=:type', 
				    'params'=>[':fk_id'=>$id, ':type'=>1 ]
			    ]);
	    
	    if (!empty($ScenarioImage)) {
		$url = $ScenarioImage->url;
	    }
	    
	    echo $url;
	    
	}
        
}
