<?php

class AdminScenariosController extends BaseAdminController {

	public function accessRules() {
		return [
			['allow', 'roles' => ['admin', 'moderator']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex() {
		$this->render('/scenarios/index', [
			'scenarios' => Scenarios::model()
						->findAll(),
		]);
	}

	public function actionEdit($id) {
	    
		/** @var User $model */
		if (!$model = Scenarios::model()
			->all()
			->findByPk($id)) {
			throw new CHttpException(404, "Scenario not found");
		};

		$class = get_class($model);
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {

			$model->attributes = $_POST[$class];

			if ($model->save()) {
				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('index'));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}
		}

		$this->render('/scenarios/add', [
			'model'     => $model,
		]);
	}

	public function actionAdd() {
		$model = new Scenarios();

		$class = get_class($model);
		
		if (Yii::app()->request->isPostRequest && isset($_POST[$class])) {
		    
			$model->attributes = $_POST[$class];

			if ($model->save()) {

				if (isset($_POST['saveAndCancel'])) {
					$this->redirect($this->createUrl('index'));
				} else {
					$this->redirect($this->createUrl('edit', ['id' => $model->id]));
				}
			}

		}

		$this->render('/scenarios/add', [
			'model'     => $model,
		]);
	}
	
	

	public function actionDelete($id) {
		echo Scenarios::model()
			->deleteByPk($id);
	}
        
}
