<?php

class BaseAdminController extends BaseController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return ['accessControl'];
	}

	public function accessRules() {
		return [
			['allow', 'roles' => ['admin', 'moderator']],
			['deny', 'users' => ['*']],
		];
	}

	protected function beforeAction($action) {
		$controllerId = Yii::app()->controller->id;
		Navigation::getMenu('main')
			->setWidget('application.widgets.Menu', ['activateItems' => true])
			->addItems([
				array(
					'id'     => 'scenarios',
					'label'  => 'Сценарии',
					'url'    => array('adminScenarios/index'),
					'active' => $controllerId == 'adminScenarios',
					'weight' => 10,
				),
				array(
					'id'     => 'scenario_steps',
					'label'  => 'Шаги',
					'url'    => array('adminScenarioSteps/index'),
					'active' => $controllerId == 'adminScenariosSteps',
					'weight' => 20,
				    
				),
				array(
					'id'     => 'users',
					'label'  => 'Пользователи',
					'url'    => array('adminUsers/index'),
					'active' => $controllerId == 'adminUsers',
					'weight' => 60,
					'visible' => Yii::app()->user->role === 'admin',
				),
				array(
					'id'      => 'sign-out',
					'label'   => '<i class="glyphicon glyphicon-off"></i> Выход',
					'url'     => array('adminUsers/signOut'),
					'active'  => false,
					'weight'  => 90,
					'HtmlOptions' => ['class' => 'df'],
					//'encodeLabel' => false,

				),
			]);
		return parent::beforeAction($action);
	}
	
	public static function saveImage($model) {
		// save image
		$filesArray = get_class($model);
		
		$topPath = strtolower($filesArray);
		switch ($filesArray) {
			case 'ScenarioImages':
				$path = '/scenarios/'  . $model->scenario_steps->scenarios->alias;
				break;
		}
		
		if (isset($_FILES[$filesArray])) {
		    
			foreach ($_FILES[$filesArray]['tmp_name'] as $imageId => $tmpName) {

				$pattern  = '/^.[^_]+_/';
		
				if ($_FILES[$filesArray]['error'][$imageId] == UPLOAD_ERR_OK) {
					$image = new FSImage($tmpName);
					$ext   = $image->getExtension();

					$finalDestination = Yii::getPathOfAlias('root') . "/public{$path}";

					FSDirectory::create($finalDestination);
	
					move_uploaded_file($tmpName, "{$finalDestination}/{$model->id}.{$ext}");
					
					switch ($filesArray) {
						case 'ScenarioImages':
							list($width, $height, $type, $attr) = getimagesize("{$finalDestination}/{$model->id}.{$ext}");
							$model->url = "{$path}/{$model->id}.{$ext}";
							$model->width = $width;
							$model->height = $height;
							$model->save();
							break;
					}
		
				}
			}
		}
	}
	
	public static function deleteImage($path) {
		$finalDestination = Yii::getPathOfAlias('root') . "/public{$path}";
		if (file_exists($finalDestination)){
			unlink($finalDestination);
		}
	}



	/**
	 * @param string $text
	 * @param null|array $options
	 * @return string
	 */
	public static function getTypographedText($text, $options = null) {
		Yii::import('ext.typograph.EMTypograph');
		return EMTypograph::fast_apply($text, $options);
	}
}
