<?php

class FileBehavior extends CActiveRecordBehavior {
	public $fileField = 'fileId';
	public $fileDeleteField = 'fileIdDelete';
	public $fileRelation = 'file';
	public $fileRelationClass = 'File';
	public $fileRelationKey = 'id';
	public $fileTypes = 'jpg, png';
	protected $oldFileId;

	public $scenarios = array('insert', 'update');

	/**
	 * @param CActiveRecord $owner
	 */
	public function attach($owner) {
		parent::attach($owner);

		$fileValidator = CValidator::createValidator('file', $owner, $this->fileField, array('types' => $this->fileTypes, 'allowEmpty' => true));
		$owner->validatorList->add($fileValidator);

		$class    = CActiveRecord::HAS_ONE;
		$relation = new $class($this->fileRelation, $this->fileRelationClass, [$this->fileRelationKey => $this->fileField]);

		$owner->getMetaData()
			->relations[$this->fileRelation] = $relation;
	}

	public function beforeSave($event) {
		/** @var $owner CActiveRecord */
		$owner = $this->getOwner();

		if (($owner->scenario == 'insert' || $owner->scenario == 'update')) {
			if ($uploadedFile = CUploadedFile::getInstance($owner, $this->fileField)) {
				$this->oldFileId = $owner->getAttribute($this->fileField);
				$owner->setAttribute($this->fileField, File::upload($uploadedFile, "/{$owner->tableName()}"));
			} else if ($owner->getAttribute($this->fileDeleteField)) {
				$this->oldFileId = $owner->getAttribute($this->fileField);
				$owner->setAttribute($this->fileField, null);
			}
		}

		return true;
	}

	public function afterSave($event) {
		if ($this->oldFileId && ($file = File::model()->findByPk($this->oldFileId))) {
			$file->delete();
		}
	}

	public function afterDelete($event) {
		$this->deleteFile();
	}

	public function deleteFile() {
		/** @var $owner CActiveRecord */
		$owner = $this->getOwner();

		if ($fileId = $owner->getAttribute($this->fileField)) {
			if ($file = File::model()->findByPk($fileId)) {
				$file->delete();
			}
		}
	}
}
