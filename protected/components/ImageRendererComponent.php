<?php

class ImageRendererComponent extends CComponent {
	protected $config = array();
	public $storageSource = 'storage';
	public $storageCached;

	public function setConfig(array $config) {
		$this->config = $config;
	}

	public function init() {
		if (empty($this->config)) {
			throw new CException(__CLASS__ . " not configured");
		}
	}

	/**
	 * @param string $size
	 * @param string|null $field
	 * @param mixed|null $default
	 * @throws CException
	 * @return CMap
	 */
	protected function getSizeConfig($size, $field = null, $default = null) {
		if (!isset($this->config[$size]) || !is_array($this->config[$size])) {
			throw new CException("Not configured size '{$size}' in " . __CLASS__);
		}

		if (!isset($this->config[$size]['width']) || !isset($this->config[$size]['height'])) {
			throw new CException("Width and Height is required in size config ({$size})");
		}

		if ($field !== null) {
			if (array_key_exists($field, $this->config[$size])) {
				if (defined($this->config[$size][$field])) {
					return constant($this->config[$size][$field]);
				}
				return $this->config[$size][$field];
			}
			return $default;
		}

		return $this->config[$size];
	}

	public function render(Image $image, $size) {
		/** @var $storage FileStorageComponent */
		if (($storage = Yii::app()->getComponent($this->storageSource)) === null) {
			throw new CHttpException(404, "Source storage component '{$this->storageSource}' not found");
		}

		$fsImage = new FSImage($storage->getFullFilename("{$image->hash}.{$image->ext}", $image->storagePath));
		$fsImage->resize(
			(int) $this->getSizeConfig($size, 'width'),
			(int) $this->getSizeConfig($size, 'height'),
			(int) $this->getSizeConfig($size, 'method', FSImage::RESIZE_METHOD_FIT),
			$this->getSizeConfig($size, 'fields', false)? FSImage::RESIZE_WITH_FIELDS: FSImage::RESIZE_WITHOUT_FIELDS,
			(float) $this->getSizeConfig($size, 'halign', 0.5),
			(float) $this->getSizeConfig($size, 'valign', 0.5),
			$this->getSizeConfig($size, 'bgcolor', FSImage::DEFAULT_BACKGROUND_COLOR)
		);

		$imageContent = $fsImage->getContent();

		if ($this->storageCached !== null) {
			/** @var $storageCached FileStorageComponent */
			if ($storageCached = Yii::app()->getComponent($this->storageCached)) {
				$yiiWebRoot = realpath(Yii::getPathOfAlias('webroot'));
				
				if (strpos(realpath($storageCached->getPath()), $yiiWebRoot) === 0) {
					$webRoot = trim(substr($storageCached->getPath(), strlen($yiiWebRoot)), '/');
					$cachedStoragePathInfo = pathinfo('/' . trim(substr(Yii::app()->request->pathInfo, strlen($webRoot)), '/'));

					$storageCached->putFileContent($imageContent, $cachedStoragePathInfo['basename'], $cachedStoragePathInfo['dirname']);

					// save cached resized images
					$thumbnailFilename = "{$cachedStoragePathInfo['dirname']}/{$cachedStoragePathInfo['basename']}";
					if (!ImageThumbnail::model()
						->findByPk([
						'imageId'  => $image->id,
						'filename' => $thumbnailFilename,
					])
					) {
						$thumbnail           = new ImageThumbnail();
						$thumbnail->imageId  = $image->id;
						$thumbnail->filename = $thumbnailFilename;
						$thumbnail->save();
					}
				}
			}
		}

		header("Content-Type: {$fsImage->getMimeType()}");
		header("Content-Length: " . strlen($imageContent));
		echo $imageContent;
		Yii::app()->end();
	}
}
